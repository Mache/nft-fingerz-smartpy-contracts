@sp.private_lambda(with_storage=None, with_operations=False, wrap_call=False)
def get_ipfs_hash(self,ipfs_uri_bytes):
    sp.set_type(ipfs_uri_bytes, sp.TBytes) 
    ipfs_uri = sp.unpack(ipfs_uri_bytes, sp.TString).open_some("ERROR UNPACKING IPFS URI")
    uri_size = abs(sp.len(ipfs_uri) - 1)
    uri_tail = sp.slice(ipfs_uri,sp.len("ipfs://"),uri_size).open_some("ERROR SLICING")
    uri_char = sp.local("uri_char","")
    uri_index = sp.local("uri_index",0)
    uri_hash = sp.local("uri_hash",[])
    separator = "/"
    sp.while (uri_char.value != separator) & (uri_index.value < uri_size):
        uri_char.value = sp.slice(uri_tail,uri_index.value,1).open_some("ERROR SLICING")
        uri_hash.value.push(uri_char.value)
        uri_index.value += 1
    sp.result(sp.concat(uri_hash.value.rev())) 