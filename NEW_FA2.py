import smartpy as sp
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/fa2_lib.py")
wanted_type = sp.TList(sp.TRecord(token_id=sp.TNat,level=sp.TNat))
on_collect_reward_type = sp.TRecord(
    reward_id=sp.TNat,
    cashed=sp.TMutez,
    pool=sp.TMutez,
    multiplied=sp.TNat,
    bonuz_jailed=sp.TList(sp.TNat)
)
collect_reward_type = sp.TRecord( 
    owner=sp.TAddress,
    wanted=wanted_type,
    bonuz=wanted_type,
    reward_ratio=sp.TNat,                                
    reward_id=sp.TNat,
    callback_contract=sp.TContract(on_collect_reward_type)
)
class Fingerz(
    FA2.Admin,
    FA2.BurnNft,
    FA2.ChangeMetadata,
    FA2.WithdrawMutez,
    #FA2.MintNft,
    FA2.OnchainviewBalanceOf,
    FA2.Fa2Nft):
    def __init__(self, admin, metadata, token_metadata = {}, ledger = {}, policy = FA2.OwnerOrOperatorTransfer(), metadata_base = None):
        FA2.Fa2Nft.__init__(self, metadata, token_metadata = token_metadata, ledger = ledger, policy = policy, metadata_base = metadata_base)
        FA2.Admin.__init__(self, admin)
        
        self.update_initial_storage(      
            next_token_id_to_reveal = 0,
            next_token_id = 0,
            salez_contract = sp.none,
            exp_multiplier=1,
            exp_hid=200,
            exp_rev=100,
            provenance_hash=sp.none, 
            exp_chart = sp.map({                
                0:sp.record(level="0",exp=0),
                1:sp.record(level="1",exp=200),
                2:sp.record(level="2",exp=300),
                3:sp.record(level="3",exp=500),
                4:sp.record(level="4",exp=800),
                5:sp.record(level="5",exp=1300),
                6:sp.record(level="6",exp=2100),
                7:sp.record(level="7",exp=3400)}, tkey=sp.TNat, tvalue=sp.TRecord(level=sp.TString,exp=sp.TNat))
                )
                
    digits = sp.map(l={x : str(x) for x in range(0, 10)}, tkey = sp.TNat, tvalue=sp.TString)
    
       
    @sp.private_lambda(with_storage=None, with_operations=False, wrap_call=False)
    def nat_stringify(self,from_nat):
        sp.set_type(from_nat,sp.TNat) 
        my_nat   = sp.local('my_nat', from_nat)
        my_string = sp.local("my_string", [])
        sp.if my_nat.value == 0:
            my_string.value.push("0")
        sp.while 0 < my_nat.value:
            my_string.value.push(self.digits[my_nat.value % 10])
            my_nat.value //= 10
        sp.result(sp.concat(my_string.value))    
    
    def manager_or_owner_only(self):
        sp.if self.data.salez_contract.open_some("salez_contract_NOT_SET") != sp.sender:
            sp.if self.data.administrator != sp.sender:
                sp.failwith("FA2_NOT_OWNER_NOR_MANAGER")

    def owner_only(self):        
        sp.if self.data.administrator != sp.sender:
            sp.failwith("FA2_NOT_OWNER")

    def manager_only(self):        
        sp.if self.data.salez_contract.open_some("SALEZ_CONTRACT_NOT_SET") != sp.sender:
            sp.failwith("FA2_NOT_OWNER_NOR_MANAGER")   

    def calculate_level(self,predator):
        sp.set_type(predator, sp.TNat)
        predator_exp = sp.unpack(self.data.token_metadata[predator].token_info["exp"], sp.TNat).open_some("ERROR UNPACKING PREDATOR EXP")
        level = sp.local("level", 0)
        sp.for exp in self.data.exp_chart.items():
            sp.if predator_exp >= exp.value.exp:
                 level.value = exp.key
                
        #sp.while (predator_exp >= self.data.exp_chart[level.value + 1].exp) & (sp.to_int(level.value) < (sp.len(self.data.exp_chart.keys()) - 1)):
        #    level.value += 1
        container_hash = sp.unpack(self.data.token_metadata[predator].token_info["container"], sp.TString).open_some("ERROR UNPACKING CONTAINER HASH")
        metadata_uri = "ipfs://"+container_hash+"/"+self.nat_stringify(predator) + ".level." + self.data.exp_chart[level.value].level + ".json"        
        self.data.token_metadata[predator].token_info["level"] = sp.pack(level.value)
        self.data.token_metadata[predator].token_info[""] = sp.pack(metadata_uri)

    @sp.entry_point(name = "default")
    def ep(self):
        sp.verify(sp.amount>sp.mutez(0), message="INSUFFICIENT FOUNDS")
  

    @sp.entry_point
    def update_provenance_hash(self, provenance_hash):
        sp.set_type(provenance_hash, sp.TString)
        self.owner_only()
        self.data.provenance_hash = sp.some(provenance_hash)

    @sp.entry_point
    def feed(self,params):
        sp.set_type(params, sp.TRecord(
            feeders = sp.TList(sp.TNat),
            predator = sp.TNat,
            owner= sp.TAddress,
            callback_contract = sp.TContract(
               sp.TList(sp.TNat)
            )
        ))
        self.manager_or_owner_only() 
        #predator_exp = sp.local("predator_exp",sp.unpack(self.data.token_metadata[params.predator].token_info["exp"], sp.TNat).open_some("ERROR UNPACKING PREDATOR EXP"))       
        sp.verify(self.data.ledger.contains(params.predator), message="PREDATOR DONT EXISTS")
        sp.verify(self.data.token_metadata.contains(params.predator), message="PREDATOR DONT EXISTS")
        sp.verify(self.data.ledger[params.predator] == params.owner, message="PREDATOR NOT OWNED")
        sp.for feeder_id in params.feeders:
            sp.if self.data.ledger.contains(feeder_id):
                sp.if self.data.ledger[feeder_id] != params.owner:
                    sp.failwith("TOKEN NOT OWNED")
            sp.else:
                sp.failwith("TOKEN DOESNT EXIST")            
        sp.for feeder_id in params.feeders:
            feeder_exp = sp.unpack(self.data.token_metadata[feeder_id].token_info["exp"], sp.TNat).open_some("ERROR UNPACKING FEEDER EXP")        
            #predator_exp.value += feeder_exp
            #sp.verify(feeder_exp > 0, message="TOKEN NOT REVEALED YET")
            predator_exp = sp.unpack(self.data.token_metadata[params.predator].token_info["exp"], sp.TNat).open_some("ERROR UNPACKING PREDATOR EXP")
            predator_next_exp = (feeder_exp * self.data.exp_multiplier) + predator_exp
            self.data.token_metadata[params.predator].token_info["exp"] = sp.pack(predator_next_exp)
            del self.data.ledger[feeder_id]
            del self.data.token_metadata[feeder_id]           
        
        self.calculate_level(params.predator)
        callbackContract = params.callback_contract                     
        sp.transfer(params.feeders,sp.mutez(0),callbackContract)

    @sp.entry_point
    def update_exp_multiplier(self,multiplier):
        sp.set_type(multiplier, sp.TNat)
        self.owner_only()
        self.data.exp_multiplier = multiplier

    @sp.entry_point
    def update_exp_base(self,exp_base):
        sp.set_type(exp_base, sp.TRecord(
            exp_hid = sp.TNat,
            exp_rev = sp.TNat
        ))
        self.owner_only()
        self.data.exp_hid = exp_base.exp_hid
        self.data.exp_rev = exp_base.exp_rev

    @sp.entry_point
    def update_exp_chart(self,exp_chart):
        sp.set_type(exp_chart, sp.TList(sp.TRecord(index=sp.TNat,level=sp.TString,exp=sp.TNat)))
        self.owner_only()
        sp.for exp in exp_chart:
            self.data.exp_chart[exp.index] = sp.record(level=exp.level, exp=exp.exp)

    @sp.entry_point
    def remove_exp_chart(self,exp_chart):
        sp.set_type(exp_chart, sp.TList(sp.TNat))
        self.owner_only()
        sp.for index in exp_chart:
            del self.data.exp_chart[index]

    @sp.entry_point
    def set_salez_contract(self,salez):
        sp.set_type(salez, sp.TAddress)
        self.owner_only()
        self.data.salez_contract = sp.some(salez)

    @sp.entry_point
    def collect_reward(self,params):
        sp.set_type(params, collect_reward_type)        
        self.manager_or_owner_only()       
        extra_reward = sp.local("extra_reward", params.reward_ratio)
        bonuz_jailed = sp.local("bonuz_jailed",sp.list(l=[],t=sp.TNat))
        total_balance = sp.balance
        sp.for fugitive in params.wanted:
            sp.if ~self.data.ledger.contains(fugitive.token_id):
                sp.failwith("TOKEN NOT MINTED")
            sp.if self.data.ledger[fugitive.token_id] != params.owner:
                sp.failwith("TOKEN NOT OWNED")
            packed_token_level = self.data.token_metadata[fugitive.token_id].token_info["level"]
            token_level = sp.unpack(packed_token_level, sp.TNat).open_some("UNPACKING TOKEN LEVEL ERROR")            
            sp.if token_level < fugitive.level:
                sp.failwith("NOT ENOUGH DANGEROUS")
        sp.for ex_fugitive in params.bonuz:
            sp.if self.data.ledger.contains(ex_fugitive.token_id):            
                sp.if (self.data.ledger[ex_fugitive.token_id] == params.owner):
                    packed_token_level = self.data.token_metadata[ex_fugitive.token_id].token_info["level"]
                    token_level = sp.unpack(packed_token_level, sp.TNat).open_some("UNPACKING TOKEN LEVEL ERROR")
                    sp.if token_level >= ex_fugitive.level:
                        bonuz_jailed.value.push(ex_fugitive.token_id)
                        extra_reward.value += params.reward_ratio
                    #sp.else:
                        #sp.failwith("NOT ENOUGH DANGEROUS")
        price = sp.utils.nat_to_mutez((sp.utils.mutez_to_nat(total_balance) * extra_reward.value) // 1000)
        sp.send(params.owner, price, message="BALANCE ERROR")
        callbackContract = params.callback_contract
        callbackData = sp.record(
            reward_id=params.reward_id,
            cashed=price, 
            pool=total_balance,
            multiplied=sp.len(bonuz_jailed.value),
            bonuz_jailed=bonuz_jailed.value
        )               
        sp.transfer(callbackData,sp.mutez(0),callbackContract)        
    
    @sp.entry_point
    def mint(self, params):
        sp.set_type(params.quantity,sp.TNat)
        sp.set_type(params.unknown_token_folder,sp.TString)
        sp.set_type(params.owner,sp.TAddress)
        self.manager_or_owner_only()
        sp.for quantity in sp.range(0,params.quantity,step=1):
            token_id = sp.compute(self.data.next_token_id)
            sp.if ~ self.data.ledger.contains(token_id):
                token_metadata_uri = "ipfs://"+params.unknown_token_folder+"/"+self.nat_stringify(token_id)+".level.0.json"
                token_info = sp.map({
                    ""  :   sp.pack(token_metadata_uri),
                    "level": sp.pack(sp.nat(0)),
                    "exp":sp.pack(self.data.exp_hid),
                    "container":sp.pack(params.unknown_token_folder)
                })
                metadata = sp.record(
                    token_id = token_id,
                    token_info = token_info,
                )
                self.data.token_metadata[token_id] = metadata
                self.data.ledger[token_id] = params.owner
                self.data.next_token_id += 1
            sp.else:
                sp.failwith("TOKEN ID ALREADY TAKEN")
        contract = sp.contract( sp.TNat,
                                self.data.salez_contract.open_some("salez_contract_NOT_SET"),
                                entry_point="on_mint").open_some()
        sp.transfer(sp.level,sp.mutez(0),contract)

    @sp.entry_point
    def reveal(self, container_folder_hash):         
        sp.set_type(container_folder_hash, sp.TString)
        self.manager_or_owner_only()
        from_ = sp.compute(self.data.next_token_id_to_reveal)
        to_exclusive = sp.compute(self.data.next_token_id)
        with sp.for_("token_id", sp.range(from_,to_exclusive,step=1)) as token_id:
            sp.if self.data.token_metadata.contains(token_id):
                token_id_stringify = self.nat_stringify(token_id)
                token_level =  sp.unpack(self.data.token_metadata[token_id].token_info["level"], sp.TNat).open_some("ERROR UNPACKING TOKEN LEVEL")         
                stringify_level = self.data.exp_chart[token_level].level
                token_info = "ipfs://"+container_folder_hash+"/"+token_id_stringify+".level."+stringify_level+".json"
                token_exp = sp.unpack(self.data.token_metadata[token_id].token_info["exp"], sp.TNat).open_some("ERROR UNPACKING TOKEN EXP")        
                sp.if token_exp == self.data.exp_hid:
                    self.data.token_metadata[token_id].token_info["exp"] = sp.pack(self.data.exp_rev)
                self.data.token_metadata[token_id].token_info[""] = sp.pack(token_info)
                self.data.token_metadata[token_id].token_info["container"] = sp.pack(container_folder_hash)
                self.data.next_token_id_to_reveal += 1
       
    @sp.entry_point
    def update_token_metadata(self, params): 
        sp.set_type(params.interval, sp.TRecord(from_=sp.TNat,to_inclusive=sp.TNat))
        sp.set_type(params.container_folder_hash, sp.TString)
        self.manager_or_owner_only()
        from_ = params.interval.from_ 
        to_ = params.interval.to_inclusive + 1
        with sp.for_("token_id", sp.range(from_,to_,step=1)) as token_id:
            sp.if self.data.token_metadata.contains(token_id):
                token_level = sp.unpack(self.data.token_metadata[token_id].token_info["level"], sp.TNat).open_some("ERROR UNPACKING TOKEN LEVEL")         
                token_level_stringify = self.data.exp_chart[token_level].level
                token_id_stringify = self.nat_stringify(token_id)        
                token_info = "ipfs://"+params.container_folder_hash+"/"+token_id_stringify+".level."+token_level_stringify+".json"
                self.data.token_metadata[token_id].token_info[""] = sp.pack(token_info)
                self.data.token_metadata[token_id].token_info["container"] = sp.pack(params.container_folder_hash)
                            
sp.add_compilation_target(
    "NFT Fingerz",
    Fingerz(
        admin=sp.address("tz1LgP5DCcgBZ6AaHQw5iwDynCi21G9MKXxy"),
        metadata = sp.utils.metadata_of_url("ipfs://bafkreihobie3dkkpwrm5izrsou3w3ch4vt7yzaemexgn37cff7bzow5aqu")
    )
)
