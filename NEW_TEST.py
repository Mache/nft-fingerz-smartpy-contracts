import smartpy as sp
FA2 = sp.io.import_script_from_url("file:/home/morcijohn/code/tezos/smartpy/smartpy_contracts/NEW_FA2.py")
SALE = sp.io.import_script_from_url("file:/home/morcijohn/code/tezos/smartpy/smartpy_contracts/NEW_SALE.py")

#FA2 = sp.io.import_stored_contract("NEW_FA2")
#SALE = sp.io.import_stored_contract("NEW_SALE")


@sp.add_test(name="Fingerz_Test")
def test():
    scenario = sp.test_scenario()
    scenario.table_of_contents()
  
    admin = sp.test_account("Administrator")
    alice = sp.test_account("Alice")
    bob   = sp.test_account("Robert")
    haxor = sp.test_account("Haxor")
    manager = sp.test_account("Manager")

    accounts = {
            "Alice":alice,
            "Admin":admin,
            "Bob":bob,
            "Haxor":haxor
        }
    scenario.h1("ACCOUNTS")
    scenario.show([admin, alice, bob, haxor, manager])
    
    unit_cost       = 7
    main_unit_cost  = unit_cost*1000000
    off_unit_cost   = unit_cost*500000
    pre_unit_cost   = unit_cost*750000
    from_date       = 1
    to_date         = 10
    now             = sp.timestamp(5)
    level           = 100
    coins_in_wallet = 2
    max_mint_per_tx = 10
    max_tokens_per_white_listed_wallet  = 10
    exc_mint_per_tx = max_mint_per_tx + 1
    exc_tokens_per_white_listed_wallet = max_tokens_per_white_listed_wallet + 1
    tokens_supply   = 15
    required_coins = 1
    add_coin = sp.list([sp.record(coin_id=0,required=1),sp.record(coin_id=1,required=1)])
    remove_coin = sp.list([1])
    remove_coin_error = sp.list([2])
    
     
    
    scenario.h1("FA2 CONTRACT INIT")
    fa2_contract = FA2.Fingerz(
        admin = admin.address,
        metadata = sp.utils.metadata_of_url("https://example.com")
    )
    scenario += fa2_contract

    scenario.h1("SALE CONTRACT INIT")
    sale_contract = SALE.Salez(
        admin = admin.address,
        fa2_contract = fa2_contract.address,
        metadata = sp.utils.metadata_of_url("https://example.com")
    )
    scenario += sale_contract  
   
    scenario.h1("SALE CONTRACT SETTUP")
    scenario.h2("set_fa2_contract by Haxor must FAIL")
    sale_contract.set_fa2_contract(fa2_contract.address).run(now=now, sender=haxor,valid=False)
    scenario.h2("set_fa2_contract by Admin must SUCCEED")
    sale_contract.set_fa2_contract(fa2_contract.address).run(now=now, sender=admin,valid=True)
    
    scenario.h2("add_managers by Haxor must FAIL")
    sale_contract.add_managers([alice.address]).run(now=now, sender=haxor,valid=False)
    scenario.h2("add_managers by Admin must SUCCEED")
    sale_contract.add_managers([manager.address,haxor.address]).run(now=now, sender=admin,valid=True)
    scenario.h2("remove_managers by Admin must SUCCEED")
    sale_contract.remove_managers([haxor.address]).run(now=now, sender=admin,valid=True)

    scenario.h2("create_coin by Haxor must FAIL")
    sale_contract.create_coin("ipfs://ZERO_COIN").run(now=now, sender=haxor, valid=False)
    scenario.h2("mint_coin by Haxor must FAIL")
    sale_contract.mint_coin(token_id=0,amount=1,to_=alice.address).run(now=now, sender=haxor, valid=False)

    scenario.h2("create_coin by Admin must SUCCEED")
    sale_contract.create_coin("ipfs://ZERO_COIN").run(now=now, sender=admin, valid=True)
    
    scenario.h2("update_coin by Haxor must FAIL")
    sale_contract.update_coin(token_id=0,token_metadata_uri="ipfs://ZERO_COINZ").run(now=now, sender=haxor, valid=False)
    scenario.h2("update_coin by Admin must SUCCEED")
    sale_contract.update_coin(token_id=0,token_metadata_uri="ipfs://ZERO_COINZ").run(now=now, sender=admin, valid=True)
    
    
    scenario.h2("add_allowed_coin by Haxor must FAIL")
    sale_contract.add_allowed_coin(add_coin).run(now=now, sender=haxor, valid=False)
    scenario.h2("add_allowed_coin by Admin must FAIL COIN DONT EXIST")
    sale_contract.add_allowed_coin(add_coin).run(now=now, sender=admin, valid=False)
    scenario.h2("create_coin by Admin must SUCCEED")
    sale_contract.create_coin("ipfs://FIRST_COINZ").run(now=now, sender=admin, valid=True)
    scenario.h2("add_allowed_coin by Admin must SUCCEED")
    sale_contract.add_allowed_coin(add_coin).run(now=now, sender=admin, valid=True)
    
    scenario.h2("remove_allowed_coin by Haxor must FAIL")
    sale_contract.remove_allowed_coin(remove_coin).run(now=now, sender=haxor, valid=False)
    scenario.h2("remove_allowed_coin by Admin must FAIL")
    sale_contract.remove_allowed_coin(remove_coin_error).run(now=now, sender=admin, valid=False)
    scenario.h2("remove_allowed_coin by Admin must SUCCEED")
    sale_contract.remove_allowed_coin(remove_coin).run(now=now, sender=admin, valid=True)

    scenario.h1("SALE CONTRACT UPDATE")
    scenario.h2("set_hidden_folder_hash by Haxor must FAIL")
    sale_contract.set_hidden_folder_hash("hidden").run(now=now, sender=haxor, valid=False)
    scenario.h2("set_hidden_folder_hash by Admin must SUCCEED")
    sale_contract.set_hidden_folder_hash("hidden").run(now=now, sender=admin, valid=True)

    scenario.h2("update_collection_max_tokens by Haxor must FAIL")
    sale_contract.update_collection_max_tokens(1000).run(now=now, sender=haxor,valid=False)
    scenario.h2("update_collection_max_tokens by Admin must SUCCEED")
    sale_contract.update_collection_max_tokens(900).run(now=now, sender=admin,valid=True)
    scenario.h2("update_collection_max_tokens by Manager must SUCCEED")
    sale_contract.update_collection_max_tokens(10000).run(now=now, sender=manager,valid=True)

    scenario.h2("update_rewards_mint_share by Haxor must FAIL")
    sale_contract.update_rewards_mint_share(100).run(now=now, sender=haxor,valid=False)
    scenario.h2("update_rewards_mint_share by Admin must SUCCEED")
    sale_contract.update_rewards_mint_share(100).run(now=now, sender=admin,valid=True)    
  
    scenario.h2("set_salez_contract for FA2 by Haxor must FAIL")
    fa2_contract.set_salez_contract(sale_contract.address).run(now=now, sender=haxor,valid=False)
    scenario.h2("set_salez_contract for FA2 by Admin must SUCCEED")
    fa2_contract.set_salez_contract(sale_contract.address).run(now=now, sender=admin,valid=True)
    
    scenario.h2("update_provenance_hash for FA2 by Haxor must FAIL")
    fa2_contract.update_provenance_hash("").run(now=now, sender=haxor,valid=False)
    scenario.h2("update_provenance_hash for FA2 by Admin must SUCCEED")
    fa2_contract.update_provenance_hash("").run(now=now, sender=admin,valid=True)
    

    scenario.h2("update_exp_multiplier by Admin must SUCCESS")
    fa2_contract.update_exp_multiplier(2).run(now=now, sender=admin,valid=True, amount=sp.mutez(0))
    scenario.h2("update_exp_base by Admin must SUCCESS")
    fa2_contract.update_exp_base(exp_hid=100,exp_rev=50).run(now=now, sender=admin,valid=True, amount=sp.mutez(0))
    scenario.h2("update_exp_chart by Admin must SUCCESS")
    fa2_contract.update_exp_chart([sp.record(index=8,level="8",exp=100)]).run(now=now, sender=admin,valid=True, amount=sp.mutez(0))
    scenario.h2("remove_exp_chart by Admin must SUCCESS")
    fa2_contract.remove_exp_chart([8]).run(now=now, sender=admin,valid=True, amount=sp.mutez(0))


   

    scenario.h1("FA2 CONTRACT ENTRY POINTS TEST")
    scenario.h2("All test must fail [NOT OWNER OR MANAGER]")
    fa2_contract.deposit_founds().run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    feed_params = sp.record(
        feeders = [0,1,2,3,4],
        predator = 5,
        owner=haxor.address,
        callback_contract = sp.contract(
            sp.TList(sp.TNat),
            sale_contract.address,
            entry_point="on_feed"
        ).open_some("ENTRY POINT DONT EXIST")
    )
    fa2_contract.feed(feed_params).run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    fa2_contract.set_salez_contract(haxor.address).run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    collect_reward_params = sp.record(
        owner=haxor.address,
        wanted=[sp.record(token_id=0,level=0)],
        bonuz=[sp.record(token_id=1,level=1)],
        reward_ratio=50,
        reward_id=0,
        callback_contract=sp.contract(
            sp.TRecord(
                reward_id=sp.TNat,
                cashed=sp.TMutez,
                pool=sp.TMutez,
                multiplied=sp.TNat,
                bonuz_jailed=sp.TList(sp.TNat)
            ),
            sale_contract.address,
            "on_collect_reward"
        ).open_some("ENTRY POINT DONT EXISTS")
    )
    fa2_contract.collect_reward(collect_reward_params).run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    mint_quantity = 10
    mint_params = sp.record(
        owner=haxor.address,
        quantity=mint_quantity,
        unknown_token_folder="HIDDEN_HAX"
    )
    fa2_contract.mint(mint_params).run(now=now, sender=haxor,valid=False)
    fa2_contract.reveal("REVEALED_HAX").run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    update_token_metadata_params = sp.record(
        interval=sp.record(
            from_=0,
            to_inclusive=10
        ),
        container_folder_hash="REVEALED_HAX_UPDATE"
    )

    fa2_contract.update_token_metadata(update_token_metadata_params).run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    fa2_contract.update_exp_multiplier(2).run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    fa2_contract.update_exp_base(exp_hid=0,exp_rev=0).run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    fa2_contract.update_exp_chart([sp.record(index=8,level="8",exp=100)]).run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))
    fa2_contract.remove_exp_chart([8]).run(now=now, sender=haxor,valid=False, amount=sp.mutez(0))

    scenario.h1("SALE CONTRACT ENTRY POINTS TEST")
    scenario.h2("All test must fail [NOT OWNER OR MANAGER]")
    mint_coint_params = sp.record(
        amount=10,
        to_=haxor.address,
        token_id=0
    )
    sale_contract.mint_coin(mint_coint_params).run(now=now, sender=haxor,valid=False)    
    sale_contract.create_coin("ipfs://HAXED_COIN").run(now=now, sender=haxor,valid=False)
    update_coin_params = sp.record(
        token_id=0,
        token_metadata_uri="ipfs://HAXED_COIN_UPDATE"
    )
    sale_contract.update_coin(update_coin_params).run(now=now, sender=haxor,valid=False)
    add_allowed_coin_params = [
        sp.record(
            coin_id=0,
            required=0
        )
    ]
    sale_contract.add_allowed_coin(add_allowed_coin_params).run(now=now, sender=haxor,valid=False)
    sale_contract.remove_allowed_coin([0]).run(now=now, sender=haxor,valid=False)
    sale_contract.on_mint(1000).run(now=now, sender=haxor,valid=False)
    create_new_phase_params = sp.record(
        name="PRESALE",
        utz_cost=sp.mutez(pre_unit_cost),
        tokens_supply=tokens_supply,
        from_date=from_date,
        to_date=to_date,
        white_listed_only=True,
        max_tokens_per_white_listed_wallet=max_tokens_per_white_listed_wallet,
        max_mint_per_tx=max_mint_per_tx
    )
    sale_contract.create_new_phase(create_new_phase_params).run(now=now, sender=haxor,valid=False)
    sale_contract.close_last_phase(True).run(now=now, sender=haxor,valid=False)
    sale_contract.add_managers([haxor.address]).run(now=now, sender=haxor,valid=False)
    sale_contract.remove_managers([admin.address]).run(now=now, sender=haxor,valid=False)
    sale_contract.update_rewards_mint_share(1000).run(now=now, sender=haxor,valid=False)
    sale_contract.set_hidden_folder_hash("HIDDEN_HAX_HASH").run(now=now, sender=haxor,valid=False)
    sale_contract.update_collection_max_tokens(1).run(now=now, sender=haxor,valid=False)
    sale_contract.set_fa2_contract(fa2_contract.address).run(now=now, sender=haxor,valid=False)
    sale_contract.add_address_to_white_list([haxor.address]).run(now=now, sender=haxor,valid=False)
    sale_contract.remove_address_from_white_list([admin.address]).run(now=now, sender=haxor,valid=False)
    #sale_contract.mint().run(now=now, sender=haxor,valid=False)
    add_rewards_params = [
        sp.record(
            wanted=[
                sp.record(
                    token_id=0,
                    level=0
                )
            ],
            bonuz=[
                sp.record(
                    token_id=1,
                    level=0
                )
            ],
            expire_on=1,
            reward_ratio=50,
            metadata="HAX"
        )
    ]
    sale_contract.add_rewards(add_rewards_params).run(now=now, sender=haxor,valid=False)
    sale_contract.remove_rewards([0]).run(now=now, sender=haxor,valid=False)
    on_collect_reward_params = sp.record(
        reward_id=0,
        cashed=sp.mutez(10),
        pool=sp.mutez(100),
        multiplied=10,
        bonuz_jailed=[1]
    )
    sale_contract.on_collect_reward(on_collect_reward_params).run(now=now, sender=haxor,valid=False)
    #sale_contract.collect_reward(0).run(now=now, sender=haxor,valid=False)
    feed_params = sp.record(
        predator=0,
        feeders=[1,2,3]
    )
    sale_contract.feed(feed_params).run(now=now, sender=haxor,valid=False)
    sale_contract.on_feed([1,2,3]).run(now=now, sender=haxor,valid=False)
    sale_contract.reveal("HAXED_REAVEAL").run(now=now, sender=haxor,valid=False)
    
    scenario.h1("PHASE SETTUP AND FORCE CLOSE")
    scenario.h2("create_new_phase by Admin must SUCCEED")
    sale_contract.create_new_phase(create_new_phase_params).run(now=now, valid=True, sender=admin)                    
    scenario.h2("close_last_phase by Admin before end date must FAIL")
    sale_contract.close_last_phase(False).run(now=now, valid=False, sender=admin)     
    scenario.h2("close_last_phase by Admin FORCE before end date must SUCCEED")
    sale_contract.close_last_phase(True).run(now=now, valid=True, sender=admin)     
        
    def mint_test(quantity, utz_amount, use_coin, sender, valid, level, info=""):
            payment = "COIN" if use_coin else "TZ"
            expect = "SUCCEED" if valid else "FAIL"        
            scenario.h2("[ "+str(level)+" block] mint " +str(quantity)+ " units by "+sender+" using "+payment+"  must " + expect + " [ " + info + " ]")
            sale_contract.mint(quantity=quantity,use_coin=use_coin, coin_id=0).run(now=now, level=level, valid=valid, sender=accounts[sender], amount=utz_amount)
            return level + 1
   
    def settup_phase_test(phase, sender, level=level):
        scenario.h1("RECHARGE "+str(coins_in_wallet)+" COIN UNITS TO " +sender+ " WALLET")    
        sale_contract.mint_coin(token_id=0,amount=coins_in_wallet,to_=accounts[sender].address).run(now=now, sender=admin, valid=True)
      
        scenario.h1("CLOSED PHASE MINT")
        mint_test(1,sp.mutez(0),True,"Haxor",False, level, "CLOSED PHASE")
        mint_test(1,sp.mutez(0),False,"Haxor",False, level, "CLOSED PHASE")

        scenario.h1("PHASE SETTUP")
        scenario.h2("create_new_phase by Haxor must FAIL")
        sale_contract.create_new_phase(phase).run(now=now, valid=False, sender=haxor)
        scenario.h2("create_new_phase by Admin must SUCCEED")
        sale_contract.create_new_phase(phase).run(now=now, valid=True, sender=admin)                    
    
        scenario.h1("WHITE LIST")
        scenario.h2("add_address_to_white_list by Haxor must FAIL")
        sale_contract.add_address_to_white_list([alice.address,bob.address,haxor.address]).run(now=now, valid=False,sender=haxor)
        scenario.h2("add_address_to_white_list by Admin must SUCCEED")
        sale_contract.add_address_to_white_list([alice.address,bob.address,haxor.address]).run(now=now, valid=True,sender=admin)
        scenario.h2("remove_address_from_white_list by Admin must SUCCEED")
        sale_contract.remove_address_from_white_list([haxor.address]).run(now=now, valid=True,sender=admin)

    def mint_phase_test(utz_cost,coins_in_wallet,required_coins,max_mint_per_tx,max_tokens_per_white_listed_wallet,tokens_supply, sender, level=level):        
        def phase_cost(quantity):
            return sp.mul(quantity,utz_cost)
        def i_phase_cost(quantity):
            return phase_cost(quantity) - sp.mutez(1)
        scenario.h1("MINT")
        max_mint_with_coins = coins_in_wallet // required_coins
        mint_test(max_mint_with_coins+1,phase_cost(0),True,sender,False, level, "INSUFFICIENT COINS")
        level += mint_test(max_mint_with_coins,phase_cost(0),True,sender,True, level)
        mint_test(max_mint_with_coins,phase_cost(0),True,sender,False, level, "EXPENDED COINS")
        mint_test(1,i_phase_cost(1),False,sender,False, level, "INSUFFICIENT TZ")
        mint_test(max_mint_per_tx + 1,phase_cost(max_mint_per_tx + 1),False,sender,False, level, "EXCEED max_mint_per_tx")
        rest = max_tokens_per_white_listed_wallet - max_mint_with_coins
        level += mint_test(rest,phase_cost(rest),False,sender,True, level)
        exceeded_supply = tokens_supply + 1
        mint_test(exceeded_supply,phase_cost(0),False,sender,False, level, "EXCEED tokens_supply")
        mint_test(exceeded_supply,phase_cost(0),True,sender,False, level, "EXCEED tokens_supply")
        return level
        
    def wl_mint_phase_test(max_tokens_per_white_listed_wallet,sender, level=level):
        scenario.h1("WHITE LIST MINT")
        mint_test(1,sp.mutez(0),True,"Haxor",False, level, "NOT WHITE LISTED") 
        mint_test(1,sp.mutez(0),False,"Haxor",False, level, "NOT WHITE LISTED") 
        mint_test(max_tokens_per_white_listed_wallet + 1,sp.mutez(0),False,sender,False, level, "EXCEED max_tokens_per_white_listed_wallet")
        mint_test(max_tokens_per_white_listed_wallet + 1,sp.mutez(0),True,sender,False, level, "EXCEED max_tokens_per_white_listed_wallet")

    def close_phase_test( sender, level=level):
        scenario.h1("PHASE CLOSE")
        scenario.h2("close_last_phase by Haxor must FAIL")
        sale_contract.close_last_phase(True).run(now=now, valid=False, sender=haxor)
        scenario.h2("close_last_phase by Admin before end date must FAIL")
        sale_contract.close_last_phase(False).run(now=now, valid=False, sender=admin)     
        
        scenario.h2("close_last_phase by Admin FORCE before end date must SUCCEED")
        sale_contract.close_last_phase(True).run(now=now, valid=True, sender=admin)     
        
        #scenario.h2("close_last_phase by Admin must SUCCEED")
        #sale_contract.close_last_phase(force=False).run(now=sp.timestamp(to_date+1), valid=True, sender=admin) 
        last_phase_id = sp.as_nat(sale_contract.data.next_phase_id - 1, "NAT ERROR")
        last_phase = sale_contract.data.phases_history[last_phase_id]
        scenario.verify(last_phase.distribution_starting_from.open_some("DISTRIBUTION ERROR")>last_phase.from_token_id )
        scenario.verify(last_phase.distribution_starting_from.open_some("DISTRIBUTION ERROR")<last_phase.from_token_id+last_phase.minted_tokens.open_some("MINTED TOKEN ERROR"))
        
        scenario.h1("CLOSED PHASE MINT")
        mint_test(1,sp.mutez(0),True,"Haxor",False, level, "CLOSED PHASE")
        mint_test(1,sp.mutez(0),False,"Haxor",False, level, "CLOSED PHASE")

    

    phase = sp.record(
        name="PRESALE",
        utz_cost=sp.mutez(pre_unit_cost),
        tokens_supply=tokens_supply,
        from_date=from_date,
        to_date=to_date,
        white_listed_only=True,
        max_tokens_per_white_listed_wallet=max_tokens_per_white_listed_wallet,
        max_mint_per_tx=max_mint_per_tx
    )
    settup_phase_test(phase,"Alice",level)
    wl_mint_phase_test(max_tokens_per_white_listed_wallet,"Alice",level)
    level = mint_phase_test(sp.mutez(pre_unit_cost),coins_in_wallet,required_coins,max_mint_per_tx,max_tokens_per_white_listed_wallet,tokens_supply,"Alice",level)
    close_phase_test("Alice",level)
   
    tokens_supply = 20
    max_tokens_per_white_listed_wallet = 20
    max_mint_per_tx = 20
    phase = sp.record(
        name="MAINSALE",
        utz_cost=sp.mutez(main_unit_cost),
        tokens_supply=tokens_supply,
        from_date=from_date,
        to_date=to_date,
        white_listed_only=False,
        max_tokens_per_white_listed_wallet=max_tokens_per_white_listed_wallet,
        max_mint_per_tx=max_mint_per_tx
    )
    settup_phase_test(phase,"Bob",level)
    level = mint_phase_test(sp.mutez(main_unit_cost),coins_in_wallet,required_coins,max_mint_per_tx,max_tokens_per_white_listed_wallet,tokens_supply,"Bob",level)
    close_phase_test("Bob",level)

    phase = sp.record(
        name="PRESALE",
        utz_cost=sp.mutez(pre_unit_cost),
        tokens_supply=tokens_supply,
        from_date=from_date,
        to_date=to_date,
        white_listed_only=True,
        max_tokens_per_white_listed_wallet=max_tokens_per_white_listed_wallet,
        max_mint_per_tx=max_mint_per_tx
    )
    settup_phase_test(phase,"Alice",level)
    wl_mint_phase_test(max_tokens_per_white_listed_wallet,"Alice",level)
    level = mint_phase_test(sp.mutez(pre_unit_cost),coins_in_wallet,required_coins,max_mint_per_tx,max_tokens_per_white_listed_wallet,tokens_supply,"Alice",level)
    close_phase_test("Alice",level)


    scenario.h1("UPDATE META")
    scenario.p("FA2 update_token_metadata")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[0].token_info[""],t=sp.TString))
    fa2_contract.update_token_metadata(interval=sp.record(from_=0,to_inclusive=0),container_folder_hash="UPATED").run(now=now, valid=False, sender=haxor)
    fa2_contract.update_token_metadata(interval=sp.record(from_=0,to_inclusive=0),container_folder_hash="UPATED").run(now=now, valid=True, sender=admin)
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[0].token_info[""],t=sp.TString))

    scenario.h1("PRE-REVEAL FEED")
    scenario.p("Predator exp pre feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["exp"],t=sp.TNat))
    scenario.p("Predator level pre feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["level"],t=sp.TNat))

    scenario.h2("feed by Alice with DOBLE EXP must SUCCEDD")
    sale_contract.feed(predator=5,feeders=[6,7,8]).run(now=now, sender=alice,valid=True)

    scenario.p("Predator exp post feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["exp"],t=sp.TNat))
    scenario.p("Predator level post feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["level"],t=sp.TNat))

    scenario.h1("METADATA TOKEN_INFO[\"\"]")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[0].token_info[""],t=sp.TString))
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[1].token_info[""],t=sp.TString))
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[2].token_info[""],t=sp.TString))
    
    scenario.h2("reveal by Haxor must FAIL")
    sale_contract.reveal("visible").run(now=now, sender = haxor, valid=False)
    scenario.h2("reveal by Haxor must FAIL")
    fa2_contract.reveal("visible").run(now=now, sender = haxor, valid=False)
    
    scenario.h2("reveal by Admin must SUCCEED")
    sale_contract.reveal("visible").run(now=now, sender = admin, valid=True)

    scenario.p("Predator exp post REVEAL:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["exp"],t=sp.TNat))
    scenario.p("Predator level post REVEAL:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["level"],t=sp.TNat))
    
    scenario.h1("METADATA TOKEN_INFO[\"\"]")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[0].token_info[""],t=sp.TString))
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[1].token_info[""],t=sp.TString))
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[2].token_info[""],t=sp.TString))
 
    scenario.h1("FEED PREDATOR")

    scenario.h2("feed by Bob with FEEDER NOT OWNED must FAIL")
    sale_contract.feed(predator=0,feeders=[1,2,3]).run(now=now, sender=bob,valid=False)

    scenario.p("Predator exp pre feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["exp"],t=sp.TNat))
    scenario.p("Predator level pre feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["level"],t=sp.TNat))
    
    scenario.h2("feed by Alice must SUCCEED")
    sale_contract.feed(predator=5,feeders=[9]).run(now=now, sender=alice,valid=True)
    
    scenario.p("Predator exp post feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["exp"],t=sp.TNat))
    scenario.p("Predator level post feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[5].token_info["level"],t=sp.TNat))

    scenario.h2("feed by Alice with FEEDER ALREADY CONSUMED must FAIL")
    sale_contract.feed(predator=5,feeders=[6,7,8,9]).run(now=now, sender=alice,valid=False)

    scenario.h1("REWARD SETTUP")
    rewards_list = [
       sp.record(
            wanted=[
                sp.record(token_id=0, level=0),
                sp.record(token_id=1, level=0),
                sp.record(token_id=2, level=0),
                sp.record(token_id=3, level=0),
                sp.record(token_id=4, level=0)],
            bonuz=[
                sp.record(token_id=5, level=1),
                sp.record(token_id=6, level=2),
                sp.record(token_id=7, level=3),
                sp.record(token_id=8, level=4),
                sp.record(token_id=9, level=5)],
            expire_on=to_date,
            reward_ratio=50, # /1000 = 0.05 = 5%,
            metadata=""
        ),
        sp.record(
            wanted=[
                sp.record(token_id=10, level=0),
                sp.record(token_id=11, level=0),
                sp.record(token_id=12, level=0),
                sp.record(token_id=13, level=0),
                sp.record(token_id=14, level=0)],
            bonuz=[
                sp.record(token_id=15, level=1),
                sp.record(token_id=16, level=2),
                sp.record(token_id=17, level=3),
                sp.record(token_id=18, level=4),
                sp.record(token_id=19, level=5)],
            expire_on=to_date,
            reward_ratio=50, #5%
            metadata=""
        ),
        sp.record(
            wanted=[
                sp.record(token_id=20, level=0),
                sp.record(token_id=21, level=0),
                sp.record(token_id=22, level=0),
                sp.record(token_id=23, level=0),
                sp.record(token_id=24, level=0)],
            bonuz=[
                sp.record(token_id=25, level=1),
                sp.record(token_id=26, level=2),
                sp.record(token_id=27, level=3),
                sp.record(token_id=28, level=4),
                sp.record(token_id=29, level=5)],
            expire_on=to_date,
            reward_ratio=50, #5%
            metadata=""
        ),
        sp.record(
            wanted=[
                sp.record(token_id=20, level=0),
                sp.record(token_id=21, level=0),
                sp.record(token_id=22, level=0),
                sp.record(token_id=23, level=0),
                sp.record(token_id=24, level=0)],
            bonuz=[
                sp.record(token_id=25, level=1),
                sp.record(token_id=26, level=2),
                sp.record(token_id=27, level=3),
                sp.record(token_id=28, level=4),
                sp.record(token_id=29, level=5)],
            expire_on=to_date,
            reward_ratio=50, #5%
            metadata=""
        ),
        sp.record(
            wanted=[
                sp.record(token_id=20, level=0),
                sp.record(token_id=21, level=0),
                sp.record(token_id=22, level=0),
                sp.record(token_id=23, level=0),
                sp.record(token_id=24, level=0)],
            bonuz=[
                sp.record(token_id=25, level=1),
                sp.record(token_id=26, level=2),
                sp.record(token_id=27, level=3),
                sp.record(token_id=28, level=4),
                sp.record(token_id=29, level=5)],
            expire_on=to_date,
            reward_ratio=50, #5%
            metadata=""
        )
    ]
    
    
    
    scenario.h2("add_rewards by Haxor must FAIL")    
    sale_contract.add_rewards(rewards_list).run(now=now, sender=haxor, valid=False)
    scenario.h2("add_rewards by Admin must SUCCEED")
    sale_contract.add_rewards(rewards_list).run(now=now, sender=admin, valid=True)

    scenario.h2("remove_rewards by Haxor must FAIL")    
    sale_contract.remove_rewards([3,4]).run(now=now, sender=haxor, valid=False)
    scenario.h2("remove_rewards by Admin must SUCCEED")
    sale_contract.remove_rewards([3,4]).run(now=now, sender=admin, valid=True)

    scenario.h2("collect_reward EXPIRED by Alice must FAIL")
    sale_contract.collect_reward(0).run(now=sp.timestamp(to_date+1), sender=alice,valid=False)
    scenario.h2("collect_reward by Alice must SUCCEED")
    sale_contract.collect_reward(0).run(now=now, sender=alice,valid=True)
    scenario.h2("collect_reward by Alice with COLLECTED PRIZE ID must FAIL")
    sale_contract.collect_reward(0).run(now=now, sender=alice, valid=False)
    scenario.h2("collect_reward by Alice with WITH TOKEN NOT OWNED must FAIL")
    sale_contract.collect_reward(1).run(now=now, sender=alice, valid=False)

    
    scenario.h1("RE-FEED")
    scenario.p("Predator exp pre feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[0].token_info["exp"],t=sp.TNat))
    scenario.p("Predator level pre feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[0].token_info["level"],t=sp.TNat))
    scenario.h2("feed by Alice must SUCCEED")    
    sale_contract.feed(predator=0, feeders=[5]).run(now=now, sender=alice,valid=True)
    scenario.p("Predator exp post feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[0].token_info["exp"],t=sp.TNat))
    scenario.p("Predator level post feed:")
    scenario.show(sp.unpack(fa2_contract.data.token_metadata[0].token_info["level"],t=sp.TNat))

    scenario.h1("BALANCE")

    scenario.h2("sale_contract.balance")
    scenario.show(sale_contract.balance)
    
    scenario.h2("withdraw_founds by Haxor must FAIL")
    sale_contract.withdraw_mutez(destination=bob.address,amount=sp.mutez(10000000)).run(now=now, sender = haxor, valid=False)
    scenario.h2("withdraw_founds by Admin must SUCCEED")
    sale_contract.withdraw_mutez(destination=bob.address,amount=sp.mutez(1000000)).run(now=now, sender = admin, valid=True)
    
    scenario.h2("sale_contract.balance")
    scenario.show(sale_contract.balance)

    scenario.h2("fa2_contract.balance")
    scenario.show(fa2_contract.balance)

    scenario.h2("deposit_founds by Alice within FA2 must SUCCEED")
    fa2_contract.deposit_founds().run(now=now, sender=alice, amount=sp.mutez(5*1000000), valid=True)
    
    scenario.h2("fa2_contract.balance")
    scenario.show(fa2_contract.balance)

    scenario.h2("transfer by Alice must SUCCEED")
    fa2_contract.transfer(sp.list([sp.record(from_=alice.address, txs=sp.list([sp.record(to_=bob.address, token_id=4, amount=1)]))])
        ).run(now=now, sender=alice,valid=True)
    scenario.h2("transfer by Alice with TOKEN NOT OWNED must FAIL")
    fa2_contract.transfer(sp.list([sp.record(from_=alice.address, txs=sp.list([sp.record(to_=bob.address, token_id=4, amount=1)]))])
        ).run(now=now, sender=alice,valid=False)
    scenario.h2("transfer by Alice with TOKEN CONSUMED must FAIL")
    fa2_contract.transfer(sp.list([sp.record(from_=alice.address, txs=sp.list([sp.record(to_=bob.address, token_id=5, amount=1)]))])
        ).run(now=now, sender=alice,valid=False)
    
    
    
    
