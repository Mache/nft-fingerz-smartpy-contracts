import smartpy as sp
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/fa2_lib.py")
wanted_type = sp.TList(sp.TRecord(token_id=sp.TNat,level=sp.TNat))
on_collect_reward_type = sp.TRecord(
    reward_id=sp.TNat,
    cashed=sp.TMutez,
    pool=sp.TMutez,
    multiplied=sp.TNat,
    bonuz_jailed=sp.TList(sp.TNat)
)
collect_reward_type = sp.TRecord( 
    owner=sp.TAddress,
    wanted=wanted_type,
    bonuz=wanted_type,
    reward_ratio=sp.TNat,                                
    reward_id=sp.TNat,
    callback_contract=sp.TContract(on_collect_reward_type)
)
reward_type = sp.TRecord(
                    wanted=wanted_type,
                    bonuz=wanted_type,
                    expire_on=sp.TNat,
                    reward_ratio=sp.TNat,
                    bonuz_jailed=sp.TOption(sp.TList(sp.TNat)),
                    multiplied=sp.TNat,
                    cashed=sp.TOption(sp.TMutez),
                    pool=sp.TOption(sp.TMutez),
                    complete=sp.TBool,
                    metadata=sp.TMap(sp.TString,sp.TBytes),
                    created=sp.TNat
                )
some_nat_type = sp.TOption(sp.TNat)
phase_type = sp.TRecord(
                    name=sp.TString,
                    utz_cost=sp.TMutez,
                    from_date=sp.TNat,
                    to_date=sp.TNat,
                    from_token_id=sp.TNat,
                    to_token_id=sp.TNat,
                    white_listed_only=sp.TBool,
                    max_tokens_per_white_listed_wallet=sp.TNat,
                    max_mint_per_tx = sp.TNat,
                    last_token_block_level=some_nat_type,
                    minted_tokens=some_nat_type,
                    distribution_starting_from=some_nat_type
                )

class Salez(
    FA2.Admin,
    FA2.ChangeMetadata,
    FA2.WithdrawMutez,
    # FA2.MintFungible,
    FA2.BurnFungible,
    FA2.OnchainviewBalanceOf,
    FA2.Fa2Fungible,
):
    def __init__(self, admin, metadata,fa2_contract,token_metadata={}, managers=[], ledger = {}, policy = FA2.OwnerOrOperatorTransfer(), metadata_base = None):
        #token_metadata=[]#[sp.map({"":sp.pack("ipfs://token_uri")})]
        FA2.Fa2Fungible.__init__(self, metadata, token_metadata = token_metadata, ledger = ledger, policy = policy, metadata_base = metadata_base)
        FA2.Admin.__init__(self, admin)
        self.update_initial_storage(  
            collection_max_tokens = sp.nat(10000), 
            next_token_id = sp.nat(0),
            next_coin_id = sp.nat(0),
            next_phase_id = sp.nat(0),
            managers = sp.set(
                l=managers,
                t=sp.TAddress
            ),         
            fa2_contract = sp.some(fa2_contract),
            hidden_folder_hash=sp.none,
            white_list=sp.map(
                l={},
                tkey=sp.TAddress,
                tvalue=sp.TList(sp.TNat)
            ),
            last_token_block_level=sp.none,
            allowed_coin=sp.none,            
            last_phase = sp.none,
            phases_history=sp.map(
                l={},
                tkey=sp.TNat,
                tvalue=phase_type),            
            rewards_mint_share = 100, # /1000 = 0.1 = 10%
            rewards_list=sp.map(
                l={},
                tkey=sp.TNat,
                tvalue=reward_type
            ),
            next_reward_id=0,
            jailed=sp.map(l={},tkey=sp.TNat,tvalue=sp.TUnit),    
            eaten=sp.map(l={},tkey=sp.TNat,tvalue=sp.TUnit)    
        )

        
    ratio_decimal_pow = 1000
    
    def get_fa2_contract(self):
        return self.data.fa2_contract.open_some(message="FA2 CONTRACT NOT DEFINED")

    def is_manager(self,sender):
        sp.set_type(sender,sp.TAddress)
        return self.data.managers.contains(sender)

    def is_managed(self,sender):
        sp.set_type(sender,sp.TAddress)
        return self.data.fa2_contract.open_some(message="FA2 CONTRACT NOT DEFINED") == sender

    def manager_or_owner_only(self):
        sp.if ~self.is_manager(sp.sender):  
            sp.if ~self.is_administrator(sp.sender):
                sp.failwith("NOT_OWNER_NOR_MANAGER")

    def owner_only(self):        
        sp.if ~self.is_administrator(sp.sender):
            sp.failwith("NOT_OWNER")

    def managed_only(self):
        sp.if ~self.is_managed(sp.sender):
            sp.failwith("NOT_FA2")   
            
    @sp.entry_point(name = "default")
    def ep():
        sp.verify(sp.amount>sp.mutez(0), message="INSUFFICIENT FOUNDS")

    @sp.entry_point
    def mint_coin(self, params):       
        self.manager_or_owner_only()
        sp.set_type(params, sp.TRecord(
            amount = sp.TNat,
            to_ = sp.TAddress,
            token_id = sp.TNat
        ))
        sp.verify(self.is_defined(params.token_id), "COIN_UNDEFINED")
        self.data.supply[params.token_id] += params.amount
        from_ = (params.to_, params.token_id)
        self.data.ledger[from_] = (
            self.data.ledger.get(from_, 0) + params.amount
        )

    @sp.entry_point
    def create_coin(self, token_metadata_uri):       
        self.owner_only()
        sp.set_type(token_metadata_uri, sp.TString)        
        self.data.supply[self.data.next_coin_id] = 0
        self.data.token_metadata[self.data.next_coin_id] = sp.record(token_id=self.data.next_coin_id,token_info=sp.map({"":sp.pack(token_metadata_uri)}))
        self.data.next_coin_id += 1

    @sp.entry_point
    def update_coin(self, params):       
        self.owner_only()
        sp.set_type(params, sp.TRecord(
            token_id=sp.TNat,
            token_metadata_uri = sp.TString
        ))        
        sp.verify(self.is_defined(params.token_id), "COIN_UNDEFINED")
        self.data.token_metadata[params.token_id] = sp.record(token_id=params.token_id,token_info=sp.map({"":sp.pack(params.token_metadata_uri)}))
        
    @sp.entry_point
    def add_allowed_coin(self, params):       
        sp.set_type(params, sp.TList(sp.TRecord(coin_id=sp.TNat,required=sp.TNat)))
        self.owner_only()
        allowed_coin = sp.local("allowed_coin",sp.map(l={},tkey=sp.TNat,tvalue=sp.TRecord(required=sp.TNat)))
        sp.if self.data.allowed_coin.is_some():
            allowed_coin.value = self.data.allowed_coin.open_some("ALLOWED COIN OPEN ERROR")
        sp.for coin in params:
            sp.verify(self.is_defined(coin.coin_id), "COIN_UNDEFINED")
        sp.for coin in params:
            allowed_coin.value[coin.coin_id] = sp.record(required=coin.required)
        self.data.allowed_coin = sp.some(allowed_coin.value)

    @sp.entry_point
    def remove_allowed_coin(self, params):       
        sp.set_type(params, sp.TList(sp.TNat))
        self.owner_only()
        allowed_coin = sp.local("allowed_coin",sp.map(l={},tkey=sp.TNat,tvalue=sp.TRecord(required=sp.TNat)))
        sp.if self.data.allowed_coin.is_some():
            allowed_coin.value = self.data.allowed_coin.open_some("ALLOWED COIN OPEN ERROR")
        sp.for coin in params:
            sp.verify(self.is_defined(coin), "COIN_UNDEFINED")
        sp.for coin in params:
            del allowed_coin.value[coin]
        sp.if sp.len(allowed_coin.value) > 0:
            self.data.allowed_coin = sp.some(allowed_coin.value)
        sp.else:
            self.data.allowed_coin = sp.none

    @sp.entry_point
    def on_mint(self,last_token_block_level):
        sp.set_type(last_token_block_level, sp.TNat)
        self.managed_only()
        self.data.last_token_block_level = sp.some(last_token_block_level)

    @sp.entry_point
    def create_new_phase(self,params):
        self.manager_or_owner_only()
        #sp.set_type(params.from_date, sp.TNat)
        sp.set_type(params, sp.TRecord(
                    name=sp.TString,
                    utz_cost=sp.TMutez,
                    tokens_supply=sp.TNat,
                    from_date=sp.TNat,
                    to_date=sp.TNat,
                    white_listed_only=sp.TBool,
                    max_tokens_per_white_listed_wallet=sp.TNat,
                    max_mint_per_tx = sp.TNat
                    )
                )
        sp.verify(~self.data.last_phase.is_some(), message="FIRST CLOSE CURRENT ACTIVE PHASE")
        #last_phase = self.data.last_phase.open_some(message="PHASE NOT DEFINED")
        to_token_id = self.data.next_token_id + params.tokens_supply
        sp.if to_token_id > self.data.collection_max_tokens:
            sp.failwith(message="SUPPLY EXCEED MAX TOKENS IN COLLECTION")
        #sp.verify(params.tokens_supply + self.data.next_token_id <= self.data.collection_max_tokens, message="")
        self.data.last_phase = sp.some(
            sp.record(                
                    name=params.name,
                    utz_cost=params.utz_cost,                
                    from_date=params.from_date,
                    to_date=params.to_date,
                    from_token_id=self.data.next_token_id,
                    to_token_id=to_token_id,
                    white_listed_only=params.white_listed_only,                
                    max_tokens_per_white_listed_wallet=params.max_tokens_per_white_listed_wallet,
                    max_mint_per_tx=params.max_mint_per_tx,
                    last_token_block_level = sp.none,
                    minted_tokens=sp.none,
                    distribution_starting_from=sp.none,
            )
        )
        
        

    @sp.entry_point
    def close_last_phase(self,force):
        sp.set_type(force, sp.TBool)
        self.manager_or_owner_only()
        sp.verify(self.data.last_phase.is_some(), message="NO ACTIVE PHASE")
        last_phase = sp.local("last_phase",self.data.last_phase.open_some(message="NO ACTIVE PHASE"))
        sp.if force:
            last_phase.value.to_date = sp.utils.seconds_of_timestamp(sp.now)
        sp.else:
            sp.verify(last_phase.value.to_date <= sp.utils.seconds_of_timestamp(sp.now), message="SALE NOT ENDED")    
        last_token_id=self.data.next_token_id
        to_token_id = sp.local("to_token_id",last_token_id)
        minted_tokens=sp.as_nat(last_token_id - last_phase.value.from_token_id)
        last_token_block_level = sp.local("last_token_block_level",sp.level)
        distribution_starting_from = sp.local("distribution_starting_from",last_token_id)
        sp.if self.data.last_token_block_level.is_some():
            last_token_block_level.value = self.data.last_token_block_level.open_some("LAST TOKEN BLOCK LEVEL NOT DEFINED")
        sp.if minted_tokens > 0:
            to_token_id.value = sp.as_nat(last_token_id - 1, "TO TOKEN ID CONVERTION ERROR") 
            distribution_starting_from.value = last_phase.value.from_token_id + (last_token_block_level.value % minted_tokens)
        last_phase.value.last_token_block_level = sp.some(last_token_block_level.value)#sp.some(self.data.last_token_block_level.open_some("LAST TOKEN BLOCK LEVEL NOT DEFINED"))
        last_phase.value.minted_tokens=sp.some(minted_tokens)
        last_phase.value.distribution_starting_from=sp.some(distribution_starting_from.value)      
        last_phase.value.to_token_id=to_token_id.value      
        self.data.phases_history[self.data.next_phase_id] = last_phase.value
        self.data.last_phase = sp.none
        self.data.next_phase_id += 1

    @sp.offchain_view()
    def get_phases_history(self):
        """PHASES HISTORY"""
        sp.result(self.data.phases_history)

    @sp.entry_point
    def add_managers(self,managers):
        self.owner_only()
        sp.set_type(managers, sp.TList(sp.TAddress))
        sp.for manager in managers:
            self.data.managers.add(manager)

    @sp.entry_point
    def remove_managers(self,managers):
        self.owner_only()
        sp.set_type(managers, sp.TList(sp.TAddress))
        sp.for manager in managers:
            self.data.managers.remove(manager)
    
    @sp.entry_point
    def update_rewards_mint_share(self,ratio):
        sp.set_type(ratio, sp.TNat)
        self.owner_only()
        self.data.rewards_mint_share = ratio

    @sp.entry_point
    def set_hidden_folder_hash(self,hidden_folder_hash):
        sp.set_type(hidden_folder_hash, sp.TString)
        self.owner_only()
        self.data.hidden_folder_hash=sp.some(hidden_folder_hash)

    @sp.entry_point
    def update_collection_max_tokens(self,collection_max_tokens):
        self.manager_or_owner_only()
        sp.set_type(collection_max_tokens, sp.TNat)
        sp.verify(collection_max_tokens > self.data.next_token_id, message="LAST MINTED TOKEN UNDER MAX TOKENS IN COLLECTION")
        self.data.collection_max_tokens = collection_max_tokens

    @sp.entry_point
    def set_fa2_contract(self,fa2_contract):
        sp.set_type(fa2_contract, sp.TAddress)
        self.owner_only()
        self.data.fa2_contract = sp.some(fa2_contract)

    @sp.entry_point
    def add_address_to_white_list(self, params):
        sp.set_type(params, sp.TList(sp.TAddress))
        self.manager_or_owner_only()
        sp.for address in params:
            self.data.white_list[address] = sp.list([],t=sp.TNat)
    
    @sp.entry_point
    def remove_address_from_white_list(self, params):
        sp.set_type(params, sp.TList(sp.TAddress))
        self.manager_or_owner_only()
        sp.for address in params:
            del self.data.white_list[address]

    @sp.entry_point
    def mint(self, params):
        sp.set_type(params.quantity, sp.TNat)
        sp.set_type(params.use_coin, sp.TBool)
        sp.set_type(params.coin_id, sp.TNat)
        sp.verify(self.data.last_phase.is_some(), message="NO ACTIVE PHASE")
        
        last_phase = self.data.last_phase.open_some("NO ACTIVE PHASE")
        sp.verify(last_phase.from_date <= sp.utils.seconds_of_timestamp(sp.now), message="SALE NOT STARTED YET")
        sp.verify(last_phase.to_date >= sp.utils.seconds_of_timestamp(sp.now), message="SALE ENDED")
        sp.verify((self.data.next_token_id + params.quantity) <= last_phase.to_token_id, message="EXCEED MAX TOKENS IN PHASE")
        sp.verify(params.quantity <= last_phase.max_mint_per_tx, message="EXCEED MAX MINT PER TX")
        sp.verify(params.quantity > 0, message="QUANTITY MUST BE MORE THAN ZERO")
        pool_share = sp.local("pool_share",sp.mutez(0))        
        sp.if last_phase.white_listed_only:
            sp.if self.data.white_list.contains(sp.sender):
                sp.if (sp.len(self.data.white_list[sp.sender]) + params.quantity) > last_phase.max_tokens_per_white_listed_wallet:
                    sp.failwith("EXCEED MAX TOKEN PER WALLET")                
            sp.else:
                sp.failwith("NOT WHITE LISTED")
        sp.if params.use_coin:
            sp.verify(self.data.allowed_coin.is_some(), message="PAY WITH COIN NOT ALLOWED")
            allowed_coin = self.data.allowed_coin.open_some("PAY WITH COIN NOT ALLOWED")
            coin_id = params.coin_id
            sp.verify(self.data.token_metadata.contains(coin_id), message="COIN DOES NOT EXIST")
            sp.verify(allowed_coin.contains(coin_id), message="COIN NOT ALLOWED")
            required = allowed_coin[coin_id].required * params.quantity
            own_coinz_supply = self.data.ledger.get((sp.sender,coin_id),0)
            sp.verify(own_coinz_supply >= required, message="NOT ENOUGHT COIN")
        sp.else:
            utz_total_cost = sp.utils.mutez_to_nat(last_phase.utz_cost) * params.quantity
            sp.verify(sp.amount >= sp.utils.nat_to_mutez(utz_total_cost), message = "TZ NOT ENOUGHT")
            pool_share.value = sp.utils.nat_to_mutez(((utz_total_cost * self.data.rewards_mint_share) // self.ratio_decimal_pow))
        data_type = sp.TRecord( unknown_token_folder=sp.TString,
                                quantity = sp.TNat,
                                owner=sp.TAddress)
        contract = sp.contract( data_type,
                                self.get_fa2_contract(),
                                entry_point="mint").open_some()
        data = sp.record(   quantity=params.quantity,
                            unknown_token_folder=self.data.hidden_folder_hash.open_some("ERROR HIDDEN FOLDER HASH"),
                            owner=sp.sender)
        sp.transfer(data,pool_share.value,contract)
        self.data.next_token_id += params.quantity      
        sp.if params.use_coin:            
            allowed_coin = self.data.allowed_coin.open_some("PAY WITH COIN NOT ALLOWED")
            coin_id = params.coin_id
            required = allowed_coin[coin_id].required * params.quantity
            ledger_id = (sp.sender, coin_id)
            self.data.ledger[ledger_id] = sp.as_nat(self.data.ledger.get(ledger_id, 0) - required, message="FA2_INSUFFICIENT_BALANCE")
            self.data.supply[coin_id] = sp.as_nat(self.data.supply.get(coin_id, 0) - required, message="FA2_INSUFFICIENT_SUPPLY")
        sp.if last_phase.white_listed_only:
            from_ = sp.as_nat(self.data.next_token_id - params.quantity, message="WHITE LIST UPDATE TOKEN FROM ERROR")
            to_ = self.data.next_token_id
            with sp.for_("token_id", sp.range(from_,to_,step=1)) as token_id:
                self.data.white_list[sp.sender].push(token_id)


    @sp.entry_point
    def add_rewards(self, rewards_list):        
        sp.set_type(rewards_list, 
            sp.TList( 
                sp.TRecord( 
                    wanted=wanted_type,
                    bonuz=wanted_type,
                    expire_on=sp.TNat,
                    reward_ratio=sp.TNat,
                    metadata=sp.TString
                )
            )
        )
        self.owner_only()
        sp.verify(~self.data.last_phase.is_some(), message="CLOSE LAST PHASE FIRST")        
        sp.for reward in rewards_list:
            data = sp.record(   wanted=reward.wanted,
                                bonuz=reward.bonuz,
                                expire_on=reward.expire_on,
                                reward_ratio=reward.reward_ratio,
                                multiplied=1,
                                bonuz_jailed=sp.none,
                                cashed=sp.none,
                                pool=sp.none,
                                complete=False,
                                metadata=sp.map({"":sp.pack(reward.metadata)}),
                                created=sp.utils.seconds_of_timestamp(sp.now)
                        )
            self.data.rewards_list[self.data.next_reward_id] = data
            self.data.next_reward_id += 1
    
    @sp.entry_point
    def remove_rewards(self, params):        
        sp.set_type(params, sp.TList(sp.TNat))
        self.owner_only()
        sp.for reward_id in params:
            del self.data.rewards_list[reward_id]

    @sp.entry_point
    def on_collect_reward(self, params):
        sp.set_type(params, sp.TRecord(
            reward_id=sp.TNat,
            cashed=sp.TMutez,
            pool=sp.TMutez,
            multiplied=sp.TNat,
            bonuz_jailed=sp.TList(sp.TNat)
            )
        )
        #sp.set_type(params.reward_id, sp.TNat)
        #sp.set_type(params.cashed, sp.TMutez)
        self.managed_only()
        reward_id = params.reward_id        
        self.data.rewards_list[reward_id].complete = True
        sp.for fugitive in self.data.rewards_list[reward_id].wanted:
            self.data.jailed[fugitive.token_id] = sp.unit
        self.data.rewards_list[reward_id].cashed = sp.some(params.cashed)
        self.data.rewards_list[reward_id].pool = sp.some(params.pool)
        sp.if params.multiplied > 0:
            self.data.rewards_list[reward_id].bonuz_jailed = sp.some(params.bonuz_jailed)
            self.data.rewards_list[reward_id].multiplied += params.multiplied
            sp.for bonuz_fugitive in params.bonuz_jailed:
                self.data.jailed[bonuz_fugitive] = sp.unit
                
    @sp.entry_point
    def collect_reward(self, reward_id):       
        sp.set_type(reward_id, sp.TNat)
        sp.verify(self.data.rewards_list.contains(reward_id), message="WANTED MISSION DOES NOT EXIST")        
        sp.verify(~self.data.rewards_list[reward_id].complete, message = "ALREADY COMPLETED")         
        sp.verify(sp.utils.seconds_of_timestamp(sp.now) <= self.data.rewards_list[reward_id].expire_on, message = "ALREADY EXPIRED")         
        
        callback_contract = sp.contract(
            on_collect_reward_type,
            sp.to_address(sp.self),
            "on_collect_reward"
        ).open_some()
        
        contract = sp.contract( collect_reward_type,
                                self.get_fa2_contract(),
                                entry_point="collect_reward").open_some()
        data = sp.record(
            owner=sp.sender,
            bonuz=self.data.rewards_list[reward_id].bonuz,
            wanted=self.data.rewards_list[reward_id].wanted,
            reward_ratio=self.data.rewards_list[reward_id].reward_ratio,
            reward_id=reward_id,
            callback_contract=callback_contract)
        sp.transfer(data,sp.mutez(0),contract)

    @sp.offchain_view()
    def get_rewards_list(self):
        """REWARDS LIST"""
        sp.result(self.data.rewards_list)

    @sp.entry_point
    def feed(self, params):       
        sp.set_type(params.feeders, sp.TList(sp.TNat))
        sp.set_type(params.predator, sp.TNat)        
        reponse_data_type = sp.TList(sp.TNat)
        callback_contract = sp.contract(
            reponse_data_type,
            sp.to_address(sp.self),
            "on_feed"
        ).open_some()
        data_type = sp.TRecord( feeders=sp.TList(sp.TNat),
                                predator=sp.TNat,
                                owner=sp.TAddress,                                
                                callback_contract=sp.TContract(reponse_data_type)
                            )
        contract = sp.contract( data_type,
                                self.get_fa2_contract(),
                                entry_point="feed").open_some()
        data = sp.record(
            feeders=params.feeders,
            predator=params.predator,
            owner=sp.sender,
            callback_contract=callback_contract)
        sp.transfer(data,sp.mutez(0),contract)

    @sp.entry_point
    def on_feed(self, feeders):
        sp.set_type(feeders, sp.TList(sp.TNat))        
        self.managed_only()        
        sp.for food in feeders:
            self.data.eaten[food] = sp.unit

    @sp.entry_point
    def reveal(self, container_folder_hash): 
        #sp.set_type(params.interval, sp.TOption(sp.TRecord(from_=sp.TNat,to_=sp.TNat)))
        #sp.set_type(params.revealed_folder_hash, sp.TString)
        self.manager_or_owner_only()
        sp.set_type(container_folder_hash, sp.TString)
        contract = sp.contract( sp.TString,
                                self.get_fa2_contract(),
                                entry_point="reveal").open_some()
        sp.transfer(container_folder_hash,sp.mutez(0),contract)

sp.add_compilation_target(
    "Salez",
    Salez(
        admin=sp.address("tz1LgP5DCcgBZ6AaHQw5iwDynCi21G9MKXxy"),
        fa2_contract=sp.address("KT1NkgDyfP2Y3bLLcvwckScVbi9hLxZRboFs"),
        metadata = sp.utils.metadata_of_url("ipfs://bafkreia2yqiqhtys2eqvndxtsuw7fjbp52f5o5aml5tbzccryytnoq5ayi")
    )
)      


    
    
   
    